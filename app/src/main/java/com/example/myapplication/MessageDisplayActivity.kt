package com.example.myapplication

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import com.example.myapplication.model.Message

class MessageDisplayActivity : AppCompatActivity() {

    private lateinit var receivedMessageTextView: AppCompatTextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message_display)

        receivedMessageTextView = findViewById(R.id.received_message)

        intent.extras?.let { extras ->
            val receivedMessage = extras.getSerializable(MESSAGE_KEY) as Message
            receivedMessageTextView.text = receivedMessage.value
        }
    }

    companion object {
        const val MESSAGE_KEY = "MESSAGE_KEY"

        fun newInstance(context: Context, message: Message): Intent {
            val bundle = Bundle()
            bundle.putSerializable(MESSAGE_KEY, message)
            val intent = Intent(context, MessageDisplayActivity::class.java)
            intent.putExtras(bundle)
            return intent
        }
    }
}