package com.example.myapplication.model

import java.io.Serializable

data class Message(val value: String) : Serializable