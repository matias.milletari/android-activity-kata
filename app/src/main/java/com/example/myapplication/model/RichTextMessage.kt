package com.example.myapplication.model

import java.io.Serializable

data class RichTextMessage(val value: String) : Serializable