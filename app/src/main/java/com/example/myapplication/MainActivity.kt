package com.example.myapplication

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import com.example.myapplication.model.Message
import com.example.myapplication.model.RichTextMessage

class MainActivity : AppCompatActivity() {

    private lateinit var messageInput : AppCompatEditText
    private lateinit var sendMessageButton : AppCompatButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        messageInput = findViewById(R.id.message_input_field)
        sendMessageButton = findViewById(R.id.send_message_button)

        sendMessageButton.setOnClickListener {
            val message = Message(value = messageInput.text.toString())
            val intent = MessageDisplayActivity.newInstance(this.applicationContext, message)
            startActivity(intent)
        }
    }
}